module.exports = getSettings

function getSettings(qs) {
  const graphSettings = getAvailableGraphs(qs); 
  const pathFinderSettings = getAvailablePathfinders(qs); 

  return {
    graphSettings,
    pathFinderSettings
  };
}


function getAvailableGraphs(qs) {
  let graphs = [
    {
      value: "amsterdam-roads",
      name: "Amsterdam"
    },
    {
      value: "seattle-roads",
      name: "Seattle"
    },
    {
      value: "rome-roads",
      name: "Rome"
    },
    {
      value: "delhi-roads",
      name: "Delhi"
    },
    {
      value: "moscow-roads",
      name: "Moscow"
    },
    {
      value: "USA-road-d.NY",
      name: "New York"
    },
    {
      value: "aix",
      name: "Aix-en-Provence"
    },
    {
      value: "aix-no-trunk",
      name: "Aix-en-Provence (sans autoroute)"
    },
    // Commenting this out, as on mobile devices it may crash the browser.
    {
      value: "tokyo-roads",
      name: "Tokyo (879K edges, 12.3 MB)"
    }
  ];

  return {
    selected: qs.get('graph'),
    graphs
  };
}

function getAvailablePathfinders(qs) {
  return {
    selected: qs.get('finder') || 'nba',
    algorithms: [{
      value: 'a-greedy-star',
      name: 'Greedy A* (suboptimal)'
    }, {
      value: 'nba',
      name: 'NBA*'
    }, {
      value: 'astar-uni',
      name: 'Unidirectional A*'
    }, {
      value: 'dijkstra',
      name: 'Dijkstra'
    }]
  };
}