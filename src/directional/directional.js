const datas = require("../../static/aix.json").elements;
let path = require("ngraph.path");
const request = require("../lib/request");
const endpoint = "static/";
const createGraph = require("ngraph.graph");
const BBox = require("../BBox");
const d3geo = require("d3-geo");

const _ = require("lodash");

const asyncFor = require("rafor");

module.exports = loadPositions;

function createProjector(lonLatBbox, r) {
  if (!r) r = 6371393; // radius of earth in meters

  var q = [0, 0];
  var projector = d3geo
    .geoMercator()
    .center([lonLatBbox.cx, lonLatBbox.cy])
    .scale(r);

  return function(lon, lat) {
    q[0] = lon;
    q[1] = lat;

    let xyPoint = projector(q);

    return {
      x: xyPoint[0],
      y: xyPoint[1]
    };
  };
}

function loadPositions(fileName, progress) {
  let graph = createGraph();
  let points = _.filter(datas, d => d.type === "node");
  let links;
  let graphBBox = new BBox();
  let lonLatBBox = new BBox();

  return setPositions()
    .then(loadLinks)
    .then(done)
    .then(() => {
      if (progress) {
        progress.linksReady = true;
        progress.pointsReady = true;
      }
    })
    .then(() => {
      return {
        graph,
        points,
        links,
        graphBBox
      };
    });

  function done() {
    points = [];
    const latLonToNodeId = new Map();
    console.log("Graph loaded. Processing...");
    let project = createProjector(lonLatBBox);

    var nodesToDelete = new Set();
    graph.forEachNode(node => {
      let data = node;

      if (!data) throw new Error("missing data for " + node.id);

      var nodeData = project(data.data.x, data.data.y);

      let prevNode = latLonToNodeId.get(data.id);
      if (prevNode) {
        prevNode = latLonToNodeId.get(data.id);
        console.log("!Marking for deletion", node.id);
        nodesToDelete.add(node.id);
      } else {
        latLonToNodeId.set(data.id, node);
      }

      node.data = nodeData;
      graphBBox.addPoint(node.data.x, node.data.y);
    });

    nodesToDelete.forEach(nodeId => {
      console.log("removing", nodeId);
      graph.removeNode(nodeId);
    });

    moveCoordinatesToZero();
    console.log(graphBBox);

    function moveCoordinatesToZero() {
      let dx = graphBBox.cx;
      let dy = graphBBox.cy;
      let movedBbox = new BBox();

      graph.forEachNode(node => {
        node.data.x = Math.round(node.data.x - dx);
        node.data.y = Math.round(node.data.y - dy);
        points.push(node.data.x, node.data.y);
        movedBbox.addPoint(node.data.x, node.data.y);
      });

      console.log("moved bbox", movedBbox);
    }
  }

  function setPositions() {
    console.log("Downloaded nodes: " + points.length / 2);

    return initNodes(points);
  }

  function initNodes(points) {
    console.time("add nodes to graph");

    return new Promise(resolve => {
      asyncFor(points, addPointToGraph, () => {
        console.timeEnd("add nodes to graph");
        resolve();
      });
    });
  }

  function addPointToGraph(data) {
    graph.addNode(data.id, { x: data.lon, y: data.lat });
    lonLatBBox.addPoint(data.lon, data.lat);
  }

  function loadLinks() {
    return setLinks();
  }

  function setLinks() {
    links = _.filter(datas, d => d.type === "way");
    console.time("add edges to graph");

    return new Promise(resolve => {
      asyncFor(links, addLinkToGraph, () => {
        console.timeEnd("add edges to graph");
        console.log(
          graph.getLinksCount() + " edges; " + graph.getNodesCount() + " nodes."
        );
        resolve();
      });
    });
  }

  function addLinkToGraph(way) {
    for (let i = 0; i < way.nodes.length - 1; i++) {
      graph.addLink(way.nodes[i], way.nodes[i + 1]);
    }
  }
}

async function main() {
    const args = [+process.argv[2], +process.argv[3]];
    const oriented = process.argv[4] !== undefined && process.argv[4] === 'true' ? true : false;
    
    if (args.length !== 2) {
        console.log('Missing args : startNode, endNode.');
        return;
    }
  const returnVars = await loadPositions("aix");

  const finder = path.nba(returnVars.graph, { oriented });
  console.log(finder.find(...args));
 //console.log(finder.find(2393805681, 2393805679));
}

main();
